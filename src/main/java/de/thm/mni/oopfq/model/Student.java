package de.thm.mni.oopfq.model;

import java.util.HashSet;
import java.util.Set;

/**
 * extends class Human by "interests" (a Set of interests)
 * @authors Jens Burgarth, Jonas Schuck
 */
public class Student extends Human {
    private Set<String> interests;

    public Student(String fname, String sname, String id, Set<String> interests) {
        super(fname, sname, id);
        this.interests = interests;
    }

    public Set<String> getInterests() {
        return interests;
    }

    public String getStudent() { return toString(); }

    public void setInterests(HashSet<String> interests) {
        this.interests = interests;
    }

    @Override
    public String toString() {
        return "{" +
                " firstname: " + getFname() + '\'' +
                " secondname: " + getSname() + '\'' +
                " id: " + getId() + '\'' +
                " interests:" + interests +
                " }";
    }
}