package de.thm.mni.oopfq.http;

import de.thm.mni.oopfq.model.Prof;
import de.thm.mni.oopfq.model.Student;
import de.thm.mni.oopfq.storage.IStore;
import de.thm.mni.oopfq.util.CJsyfieable;
import de.thm.mni.oopfq.util.Creation;
import de.thm.mni.oopfq.util.InputCheck;
import de.thm.mni.oopfq.util.MentorMatch;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.lang.*;
import java.util.*;

/**
 * Provides an web api for to handle http calls.
 * @authors Jens Burgarth, Jonas Schuck
 */
public class WebApi {
    private Vertx vertx;
    private Set<Prof> professorSet;
    private Set<Student> studentSet;
    CJsyfieable cJsyfieable = new CJsyfieable();

    /**
     * @param vertx The current vertex application.
     * @param store A store to store data.
     */
    public WebApi(Vertx vertx, IStore store) {
        this.vertx = vertx;

        this.professorSet = store.newStore();
        this.studentSet = store.newStore();
    }

    /**
     * @return The http router for the api.
     */
    public Router getRouter() {
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        router.post("/professors").handler(this::createProfessor);
        router.get("/professors").handler(this::listProfessors);

        router.get("/professors/:id").handler(this::getProfessor);
        router.put("/professors/:id").handler(this::changeProfessor);
        router.delete("/professors/:id").handler(this::deleteProfessor);

        router.post("/students").handler(this::createStudent);
        router.get("/students").handler(this::listStudents);

        router.get("/students/:id").handler(this::getStudent);
        router.put("/students/:id").handler(this::changeStudent);
        router.delete("/students/:id").handler(this::deleteStudent);

        router.get("/mentorings").handler(this::listMentors);

        return router;
    }


    /**
     * Handles over input from POST body to Method createP in class Creation
     * @param routingContext contains POST body data
     * @return returns URL to get the created Prof, if creation fails returns IllegalArgumentException
     */
    protected void createProfessor(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        String id = body.getString("id");
        HttpServerResponse response = routingContext.response();
        Creation creation = new Creation();
        try {
            professorSet = creation.createP(body, professorSet);
            JsonObject newProf = cJsyfieable.toJson().put("Prof created", "/professors/" + id);
            response.putHeader("content-type", "application/json").end(newProf.encode());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end(e.getMessage());
        }
    }


    /**
     * @pre professorSet must contain at least one Prof object
     * @response a list of all created Profs
     */
    protected void listProfessors(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();

        try {
            InputCheck.isEmpty(professorSet);

            JsonObject newProfList = cJsyfieable.toJson().put("list of all profs", professorSet.toString());
            response.putHeader("content-type", "application/json").end(newProfList.encode());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(412).end("Precondition Failed: No profs found.");

        }
    }


    /**
     * @pre requested id matches a created Profs id
     * @param routingContext contains id of requested Prof
     * @return returns all data of the matching Prof object
     */
    protected void getProfessor(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        String id = routingContext.request().getParam("id");

        try {
            InputCheck.isEmpty(id);
            String outputProf = InputCheck.isSameId(id, professorSet);
            response.putHeader("content-type", "application/json");
            JsonObject newProf = cJsyfieable.toJson().put("requested prof", outputProf);
            response.end(newProf.encode());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end(e.getMessage());

        } catch (NoSuchElementException e) {
            response.setStatusCode(404).end("Prof with ID " + id + " not found.");
        }
    }


    /**
     * deletes Prof where Profs id matches input id
     * creates new Prof based on the input data
     * @pre requested id matches a created Profs id
     * @pre new id matches old id
     * @param routingContext contains all request data
     * @response all data of the matching Student object
     */
    protected void changeProfessor(RoutingContext routingContext) {
        Creation creation = new Creation();
        JsonObject body = routingContext.getBodyAsJson();
        String id = routingContext.request().getParam("id");
        HttpServerResponse response = routingContext.response();


        try {
            InputCheck.isEmpty(body.getString("fname"));
            InputCheck.isEmpty(body.getString("sname"));
            String bodyID = InputCheck.isEmpty(body.getString("id"));
            InputCheck.isEmpty(body.getString("office"));
            InputCheck.isEmpty(body.getJsonArray("competencies"));
            if (id.equals(bodyID)) {
                if (professorSet.removeIf(p -> p.getId().equals(id))) {
                    professorSet = creation.createP(body, professorSet);

                    String outputProf = InputCheck.isSameId(id, professorSet);
                    response.putHeader("content-type", "application/json");
                    JsonObject newProf = cJsyfieable.toJson().put("updated prof", outputProf);
                    response.end(newProf.encode());
                }
                throw new NoSuchElementException();
            }
            throw new IllegalArgumentException("Changing the ID is not allowed.");

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end(e.getMessage());
        } catch (NoSuchElementException e) {
            response.setStatusCode(404).end("Changing failed: prof with ID " + id + " not found.");
        }
    }


    /**
     * deletes Student where Students id matches input id
     * @pre requested id matches a created Students id
     * @param routingContext contains requested id for delete
     * @response id of the deleted Student
     */
    protected void deleteProfessor(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        HttpServerResponse response = routingContext.response();

        try  {
            InputCheck.isEmpty(id);
            if (professorSet.removeIf(p -> p.getId().equals(id))) {
                JsonObject deleteProf = cJsyfieable.toJson().put("Deleted prof", "/professors/" + id);
                response.putHeader("content-type", "application/json").end(deleteProf.encode());
            }
            throw new NoSuchElementException("Prof with ID " + id + " not found.");

        } catch (NoSuchElementException e) {
            response.setStatusCode(404).end(e.getMessage());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end("Pleaser enter a valid ID.");
        }
    }




    /**
     * Handles over input from POST body to Method createS in class Creation
     * @param routingContext contains POST body data
     * @return returns URL to get the created Student, if creation fails returns IllegalArgumentException
     */
    protected void createStudent(RoutingContext routingContext) {
        JsonObject body = routingContext.getBodyAsJson();
        String id = body.getString("id");
        HttpServerResponse response = routingContext.response();
        Creation creation = new Creation();
        try {
            InputCheck.isEmpty(id);
            studentSet = creation.createS(body, studentSet);
            JsonObject newStudent = cJsyfieable.toJson().put("Student created", "/students/" + id);
            response.putHeader("content-type", "application/json").end(newStudent.encode());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end(e.getMessage());
        }
    }


    /**
     * @pre studentSet must contain at least one Student object
     * @response a list of all created Students
     */
    protected void listStudents(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();

        try {
            InputCheck.isEmpty(studentSet);
            JsonObject newStudentList = cJsyfieable.toJson().put("list of all profs", studentSet.toString());
            response.putHeader("content-type", "application/json").end(newStudentList.encode());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(412).end("Precondition Failed: No students found.");
        }
    }


    /**
     * @pre requested id matches a created Students id
     * @param routingContext contains id of requested Student
     * @response all data of the matching Student object
     */
    protected void getStudent(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        HttpServerResponse response = routingContext.response();
        try {
            InputCheck.isEmpty(id);
            String outputStudent = InputCheck.isSameId(id, studentSet);
            response.putHeader("content-type", "application/json");
            JsonObject newStudent = cJsyfieable.toJson().put("requested student", outputStudent);
            response.end(newStudent.encode());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end(e.getMessage());

        } catch (NoSuchElementException e) {
            response.setStatusCode(404).end("student with ID " + id + " not found.");
        }
    }


    /**
     * deletes Student where Students id matches input id
     * creates new Student based on the input data
     * @pre requested id matches a created Students id
     * @pre new id matches old id
     * @param routingContext contains all request data
     * @response all data of the matching Student object
     */
    protected void changeStudent(RoutingContext routingContext) {
        Creation creation = new Creation();
        JsonObject body = routingContext.getBodyAsJson();
        String id = routingContext.request().getParam("id");
        HttpServerResponse response = routingContext.response();
        try {
            InputCheck.isEmpty(body.getString("fname"));
            InputCheck.isEmpty(body.getString("sname"));
            String bodyID = InputCheck.isEmpty(body.getString("id"));
            InputCheck.isEmpty(body.getJsonArray("interests"));

            if(id.equals(bodyID)) {
                if (studentSet.removeIf(s -> s.getId().equals(id))) {
                    studentSet = creation.createS(body, studentSet);

                    String outputStudent = InputCheck.isSameId(id, studentSet);
                    response.putHeader("content-type", "application/json");
                    JsonObject newStudent = cJsyfieable.toJson().put("updated student", outputStudent);
                    response.end(newStudent.encode());
                }
                throw new NoSuchElementException();
            }
            throw new IllegalArgumentException("Changing the ID is not allowed.");

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end(e.getMessage());
        } catch (NoSuchElementException e) {
            response.setStatusCode(404).end("Changing failed: student with ID " + id + " not found.");
        }
    }


    /**
     * deletes Student where Students id matches input id
     * @pre requested id matches a created Students id
     * @param routingContext contains requested id for delete
     * @response id of the deleted Student
     */
    protected void deleteStudent(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        HttpServerResponse response = routingContext.response();

        try {
            if (studentSet.removeIf(s -> s.getId().equals(id))) {
                JsonObject deleteStudent = cJsyfieable.toJson().put("Deleted student", "/students/" + id);
                response.putHeader("content-type", "application/json").end(deleteStudent.encode());
            }
            throw new NoSuchElementException("Student with ID " + id + " not found.");


        } catch (NoSuchElementException e) {
            response.setStatusCode(404).end(e.getMessage());

        } catch (IllegalArgumentException e) {
            response.setStatusCode(400).end("Pleaser enter a valid ID.");
        }
    }


    /**
     * calls class MentorMatch for further operations
     * @reponse set of students matched to one professor per student
     */
    protected void listMentors(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        try {
            MentorMatch mentoring = new MentorMatch(InputCheck.isEmpty(studentSet), InputCheck.isEmpty(professorSet));
            JsonObject mentorMatching = cJsyfieable.toJson().put("students and their mentors ", mentoring.getMentorMatch().toString());
            response.putHeader("content-type", "application/json").end(mentorMatching.encode());
        } catch (IllegalArgumentException e) {
            response.setStatusCode(412).end("Precondition Failed: No students or profs found.");
        }
    }
}
