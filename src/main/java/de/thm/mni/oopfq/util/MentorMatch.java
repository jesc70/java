package de.thm.mni.oopfq.util;

import de.thm.mni.oopfq.model.Prof;
import de.thm.mni.oopfq.model.Student;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

/**
 * Provides a method to match Student and Prof objects
 * @authors Jens Burgarth, Jonas Schuck
 */
public class MentorMatch {
    private Set<Student> studentSet;
    private Set<Prof> professorSet;

    public MentorMatch(Set<Student> studentSet, Set<Prof> professorSet) {
        this.studentSet = studentSet;
        this.professorSet = professorSet;
    }
    /**
     * Matches Student and Prof objects based on comparison of their interests and competencies
     * Every Student gets matched with one Prof, depending on the highest matching score
     * If multiple Profs get the same score for one Student, the Student gets matched with the Prof that
     * has the least Students matched to
     * @pre at least one entry in both studentSet and professorSet
     * @returns HashMap of all Student names matched with one Prof name per Student
     */
    public HashMap<String, String> getMentorMatch() {
        HashMap<String, String> mentoring = new HashMap<String, String>();
        HashMap<String, Integer> profWorkload = new HashMap<String, Integer>();
        // initializes every Profs workload with 0
        for (Prof p : professorSet) {
            profWorkload.put(p.getId(), 0);
        }
        String prof = professorSet.iterator().next().getId();
        //iterates over every Student in studentSet
        for (Student s : studentSet) {
            int highcount = 0;
            int matching;
            //iterates over every Prof in professorSet
            for (Prof p : professorSet) {
                matching = 0;
                String[] interests = s.getInterests().toArray(new String[s.getInterests().size()]);
                Arrays.sort(interests);
                String[] competencies = p.getCompetencies().toArray(new String[p.getCompetencies().size()]);
                Arrays.sort(competencies);
                for (int i = 0; i < interests.length; i++) {
                    for (int j = 0; j < competencies.length; j++) {
                        if (interests[i].equals(competencies[j])) {
                            matching++;
                        }
                    }
                }
                if (matching > highcount) {
                    highcount = matching;
                    prof = p.getId();
                } else if (highcount == matching) {
                    if (profWorkload.get(prof) > profWorkload.get(p.getId())) {
                        prof = p.getId();
                    }
                }
            }
            String profName = "";
            for (Prof p : professorSet) {
                if(p.getId().equals(prof)) {
                    profName = p.getFname() + " " + p.getSname();
                    break;
                }
            }

            mentoring.put("Student: " + s.getFname() + " " +  s.getSname(), "Prof: " + profName);
            profWorkload.put(prof, profWorkload.get(prof) + 1);

        }

    return mentoring;

    }
}