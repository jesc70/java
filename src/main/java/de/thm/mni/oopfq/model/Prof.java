package de.thm.mni.oopfq.model;

import java.util.Set;

/**
 * extends class Human by "competencies" (a Set of competencies) and a String "office"
 * @authors Jens Burgarth, Jonas Schuck
 */
public class Prof extends Human{
    private String office;
    private Set<String> competencies;


    public Prof(String fname, String sname, String id, String office, Set<String> competencies) {
        super(fname, sname, id);
        this.office = office;
        this.competencies = competencies;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public void setCompetencies(Set<String> competencies) {
        this.competencies = competencies;
    }

    public String getOffice() {
        return office;
    }

    public Set<String> getCompetencies() {
        return competencies;
    }

    public String getProf() {
        return toString();
    }

    @Override
    public String toString() {
        return "{" +
                " firstname: " + getFname() + '\'' +
                " secondname: " + getSname() + '\'' +
                " id: " + getId() + '\'' +
                " office: " + getOffice() + '\'' +
                " competencies: " + getCompetencies() +
                " }";
    }
}

