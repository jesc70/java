package de.thm.mni.oopfq.util;

import java.lang.*;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Provides methods to verify user inputs
 * @authors Jens Burgarth, Jonas Schuck
 */
public class InputCheck {

    /**
     * Checks if an entered value contains anything but whitespaces or "[", "]"
     * @throws IllegalArgumentException if value is empty
     * @param value contains an input value from Type T
     * @returns entered value
     */
    public static <T> T isEmpty(T value) throws IllegalArgumentException {
        String rValue = value.toString().replace("[", "").replace("]", "")
                .replace(",", "").replace("\"", "").trim();
        if (rValue.length() == 0) {
            throw new IllegalArgumentException("Enter an input for every field.");
        } else {
            return value;
        }
    }

    /**
     * Checks if an entered String value is contained in an entered Set
     * @throws IllegalArgumentException if value is not in entered Set
     * @param value contains a String
     * @param valueSet contains a Set
     * @returns object as String from entered valueSet
     */
    public static <T> String isSameId(String value, Set<T> valueSet) throws IllegalArgumentException {
       for (T s : valueSet) {
            if (s.toString().contains(value)) {
                return s.toString();
            }
        } throw new NoSuchElementException("User not Found.");
    }
}
