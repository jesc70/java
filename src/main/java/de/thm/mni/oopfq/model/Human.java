package de.thm.mni.oopfq.model;

/**
 * foundational Class that describes every human connected to the application
 * grants every human the Strings "fname" (for their first name), "sname" (for their second/last name) and
 * "id" (as an unique identifier)
 * @authors Jens Burgarth, Jonas Schuck
 */

public abstract class Human {
    private String fname;
    private String sname;
    private String id;

    public Human(String fname, String sname, String id) {
        this.fname = fname;
        this.sname = sname;
        this.id = id;
    }

    public String getFname() { return fname; }

    public String getSname() {
        return sname;
    }

    public String getId() {
        return id;
    }
}
