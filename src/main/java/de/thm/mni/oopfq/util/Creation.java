package de.thm.mni.oopfq.util;

import de.thm.mni.oopfq.model.Prof;
import de.thm.mni.oopfq.model.Student;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.HashSet;
import java.util.Set;

/**
 * Provides methods to create new objects
 * @authors Jens Burgarth, Jonas Schuck
 */
public class Creation {
    /**
     * Creates new Student object, adds this object to the studentSet
     * @pre for every variable in Student there must be at least one value in body
     * @param studentSet contains all created Student objects
     * @param body contains all data for the creation of the new Student object
     * @returns studentSet with the new created Student object
     */
    public static Set<Student> createS(JsonObject body, Set<Student> studentSet) throws IllegalArgumentException {
        String fname = InputCheck.isEmpty(body.getString("fname"));
        String sname = InputCheck.isEmpty(body.getString("sname"));
        String id = InputCheck.isEmpty(body.getString("id"));
        Set<String> interestsSet = new HashSet<>();

        JsonArray interests = InputCheck.isEmpty(body.getJsonArray("interests"));

        for (int i = 0; i < interests.size(); i++) {
            interestsSet.add(interests.getString(i));
        }

        Student student = new Student(
                fname, sname, id, interestsSet
        );

        for(Student s : studentSet) {
            if(s.getId().equals(id)) {
                throw new IllegalArgumentException("Student is already existing.");
            }
        }
        studentSet.add(student);

        return studentSet;
    }

    /**
     * Creates new Prof object, adds this object to the professorSet
     * @pre for every variable in Prof there must be at least one value in body
     * @param professorSet contains all created Student objects
     * @param body contains all data for the creation of the new Prof object
     * @returns professorSet with the new created Prof object
     */
    public static Set<Prof> createP(JsonObject body, Set<Prof> professorSet) throws IllegalArgumentException {
        String fname = InputCheck.isEmpty(body.getString("fname"));
        String sname = InputCheck.isEmpty(body.getString("sname"));
        String id = InputCheck.isEmpty(body.getString("id"));
        Set<String> competenciesSet = new HashSet<>();
        JsonArray competencies = InputCheck.isEmpty(body.getJsonArray("competencies"));
        String office = InputCheck.isEmpty(body.getString("office"));

        for (Prof p : professorSet) {
            if (p.getId().equals(id)) {
                throw new IllegalArgumentException("Prof is already existing.");    //@throws Exception professor ID equals id from body
            }
        }

        for (int i = 0; i < competencies.size(); i++) {
            competenciesSet.add(competencies.getString(i));
        }

        Prof professor = new Prof(
                fname, sname, id, office, competenciesSet
        );

        professorSet.add(professor);

        return professorSet;
    }
}
